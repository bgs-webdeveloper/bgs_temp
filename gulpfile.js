var gulp = require('gulp'),
    sass = require('gulp-sass'), //Sass plugin
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglifyjs'),
    gutil = require('gulp-util'),
    dirSep = require('path').sep,
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin'),
    filesize = require('gulp-filesize'),
    cache = require('gulp-cache'),
    browserSync = require('browser-sync'), // Подключаем Browser Sync
    rigger = require('gulp-rigger'); //Плагин позволяет импортировать один файл в другой простой конструкцией //= footer.html


gulp.task('sass', function(){
    return gulp.src('src/scss/**/*.+(scss|sass)')
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

gulp.task('autoprefixer', function(){
   gulp.src('src/*.css')
       .pipe(autoprefixer({
           browsers: ['last 2 versions'],
          cascade: false
        }))
        .pipe(gulp.dest('build'))
});

gulp.task('modules-css', function(){
    return gulp.src('src/modules/**/*.scss')
        .pipe(sass())
        .pipe(concat('module.css'))
        .pipe(gulp.dest('src/css/'))
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});
gulp.task('modules-js', function(){
    return gulp.src('src/modules/**/*.js')
        .pipe(concat('module.js'))
        .pipe(gulp.dest('src/js/'))
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'src' // Директория для сервера - src
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('watch', ['browser-sync', 'sass', 'autoprefixer', 'modules-css', 'modules-js'], function() {
    gulp.watch('src/**/*.scss', ['sass']); // Наблюдение за sass файлами
    gulp.watch('src/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('src/**/*.js', browserSync.reload); // Наблюдение за JS файлами в папке js
});

gulp.task('default', ['watch']);


//BUILD

    //Минимифицирование изображения
    gulp.task('img', function() {
        return gulp.src('src/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
        })))
        .pipe(gulp.dest('build/img'));
    });
    //Размер файлов после минификации
    gulp.task('filesize', function() {  
        return gulp.src('build/js/module.js')
            /*.pipe(gulp.dest('build/js'))*/
            .pipe(filesize())
            .pipe(uglify())
            .pipe(rename('main.min.js'))
            .pipe(gulp.dest('build/js'))
            .pipe(filesize())
            .on('error', gutil.log)
    });

    //Очистка папки build
    gulp.task('clean', function(){
        return gulp.src('build/*', {read: true})
            .pipe(clean());
    });

    //Создание папки build
    gulp.task('build', ['clean'], function(){
        //Создание CSS
        gulp.src('src/css/*.css')
            .pipe(gulp.dest('build/css'));
        
        //Создание JS
        gulp.src('src/js/jquery-3.3.1.min.js')
            .pipe(gulp.dest('build/js'));
        gulp.src(['src/js/module.js', 'src/js/main.js'])
            .pipe(gulp.dest('build/js'))
            .pipe(uglify())
            .pipe(rename('main.min.js'))
            .pipe(gulp.dest('build/js'));
        
        //Создание FONTS
        gulp.src('src/fonts/*')
            .pipe(gulp.dest('build/fonts'));
        
        //Создание PLUGINS
        gulp.src('src/plugins/**/*')
            .pipe(gulp.dest('build/plugins'));
        
        //Создание HTML
        gulp.src('src/*.html') 
            .pipe(gulp.dest('build'));
        gulp.start('img');
        gulp.start('filesize')
    });

//end BUILD